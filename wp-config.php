<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the website, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fsjwebdb' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'db' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5yV%|c]hwgzck5<,lA%A2q ,-#TpomDXs!Tt1Od9Rczd0=-Nz[a`n>-{IFpp)fxm' );
define( 'SECURE_AUTH_KEY',  '-!8Gvm1Vtx,fsw2FO?++]-zbLOX{f)Qkf4zJPuoB_v:FXN(}lnj#bLU~f@:sl{cG' );
define( 'LOGGED_IN_KEY',    'K8uMGjoqP11{^uR)#sGbCy!X+aH:P~31Z9nVeuM~Ng51gnvjJj_$.Uz:cP%upxo4' );
define( 'NONCE_KEY',        'R]&2X;ct,oy[9Y6gEx!ap07eEdsSoA34{rqPqbFj*-0_%?W2-Alb|S+<4d1}`>^n' );
define( 'AUTH_SALT',        'tJ,38Vz)fDNvBF#0sl>7^uZx$[_#;{Ckes5WT{tZS`5R2B]N,>KO!=L>W/hi9F6)' );
define( 'SECURE_AUTH_SALT', 'Bpfb{s(cPaO+-jxVOK:=FTCcbp6e?0YB}E<{]|k2^[lDKOmh6+f!hHGWxc*Z1#Y>' );
define( 'LOGGED_IN_SALT',   '3(&5M~g49?EG<G)v-O.Vr8;$j;+7J)%uPokE`Te+$E}Iy-)#*FL0ZK(l|E}kOAkH' );
define( 'NONCE_SALT',       '@GN4j`&I|2{al@T@rGg#3QvB$4AZ-q?{!|8+^-tVjdoH|7t&.z%f7/M{E~6B4$4j' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
